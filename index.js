module.exports = {
    'singleQuote': true,
    'jsxBracketSameLine': false,
    'trailingComma': 'all',
    'bracketSpacing': false,
    'printWidth': 90,
}
